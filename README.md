# Personnaliser VIM

Quelques configurations de base à mettre en place pour mieux utiliser VIM et le rendre un peu plus sexy.


# Installation

Pour configurer et personnaliser votre VIM, commencez par ouvrir un fichier en
tapant cette ligne de commande dans le terminal :

```
vim ~/.vimrc
```

Puis copier les exemples de configuration que vous souhaitez utiliser (cf. : .vimrc).


# Utilisation de quelques commandes en ligne

* Touche I = pour faire une insertion
* Touche Echap = pour quitter le mode insertion
* `:q` = pour quitter
* `:w` = pour enregistrer
* `:wq` = raccourci pour enregistrer et quitter
* `:x` = autre raccourci pour enregistrer et quitter