" @link https://www.plpeeters.com/blog/fr/post/13-editeur-vim-introduction
" @link https://scotch.io/tutorials/getting-started-with-vim-an-interactive-guide

" Active l'utilisation de la souris
set mouse=a

" Affiche les commandes dans le coin inférieur droit
set showcmd

" Active la détection du type de fichier en fonction de son extension,
" nécessaire pour la coloration syntaxique et l'indentation automatique
filetype plugin indent on

" Active la coloration syntaxique
syntax on

" Active l'indentation automatique
set autoindent

" Active les numéros de ligne
set nu

" Définit la largeur d'un tab à 4 colonnes
set tabstop=4

" Définit la largeur de l'indentation automatique qui doit être utiliser
set shiftwidth=4

" Permet de toujours laisser trois lignes visibles au-dessus et en-dessous du
" curseur
set scrolloff=3
